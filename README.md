# What is this ?

This contains a list of all domains in blacklist and whitelists for my local network. This has been curated from multiple sources and will helps in making sure that I have access to these lists at all times and facilitate version control on the same, I have lost some lists previously and hence this.

## Blacklists

This part contains all our blacklists.

### Advertising

```txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/AdguardDNS.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/Admiral.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/Easylist.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/adservers.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/hosts
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/hosts.1
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/hosts.2
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/hosts.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/serverslist.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/adverts/simple_ad.txt
```

### Malicious

```txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/Airelle-hrsk.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/AntiMalwareHosts.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/Mandiant_APT1_Report_Appendix_D.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/Prigent-Crypto.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/Prigent-Malware.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/Shalla-mal.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/blacklist.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/hosts
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/immortal_domains.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/main-blacklist.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/notrack-malware.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/phishing_army_blocklist_extended.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/simple_malvertising.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/malicious/url-lists.txt
```

### Suspicious

```txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/BillStearns.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/SNAFU.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/dom-bl-base.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts-file.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts-file2.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts.1
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts.2
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts.txt.1
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/hosts0.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/neohostsbasic.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/spammers.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/suspicious/w3kbl.txt
```

### Tracking and Telemetry

```txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/Airelle-trc.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/AmazonFireTV.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/Easyprivacy.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/Prigent-Ads.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/SmartTV.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/TOP_EU_US_Ads_Trackers_HOST
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/ads-and-tracking-extended.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/android-tracking.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/firstparty-trackers-hosts.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/hosts
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/multiparty-trackers-hosts.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/notrack-blocklist.txt
https://gitlab.com/samuhika/dns-blocklist/-/raw/main/tracking_and_telemetry/spy.txt
```

## Sources

Source of the lists can be found in `source` folder in this repository. Credits to the original authors, I am merely curating it for my comfort.